import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from 'react-bootstrap';
import { Outlet, useNavigate } from 'react-router-dom';



export default function Home() {
    const navigate = useNavigate();
    const [employee, setEmployee] = useState([]);

    useEffect(() => {
        fetch('https://64952386b08e17c91791adac.mockapi.io/emp')
            .then(res => res.json())
            .then(data => setEmployee(data))
    }, []);

    return (
        <div className="home-container">
            <Button variant="success" onClick={() => { navigate(`empadd`) }}>Add Employee</Button>
            <Row className="py-3">
                {employee && employee.length > 0 ? (
                    employee.map(emp => (
                        <Col md={4} key={emp.id} className='py-3'>
                            <Card className="employee-card">
                                <Card.Img variant="top" src={emp.avatar} />
                                <Card.Body>
                                    <Card.Title className="employee-name">{emp.name}</Card.Title>
                                    <Button variant="primary" onClick={() => { navigate(`empdetails/${emp.id}`) }}>See Details</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))
                ) : (
                    <p>Loading employee data...</p>
                )}
            </Row>
        </div>
    )
}
