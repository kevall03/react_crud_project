import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Container from "react-bootstrap/Container";

export default function EmployeeEdit() {
    const [employee, setEmployee] = useState({});
    const navigate = useNavigate();
    const { id } = useParams();

    useEffect(() => {
        console.log('Fetching employee data for ID:', id);
        fetch(`https://64952386b08e17c91791adac.mockapi.io/emp/${id}`)
            .then(res => res.json())
            .then(data => {
                console.log('Fetched employee data:', data);
                setEmployee(data);
            })
    }, [id]);

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch(`https://64952386b08e17c91791adac.mockapi.io/emp/${id}`, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(employee)
        })
            .then(() => {
                navigate("/");
            })
            .catch((error) => {
                console.log(error);
            })
    }

    if (!employee) {
        return <div>Loading...</div>;
    }

    return (
        <Container className="d-flex justify-content-center">
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={employee.avatar} />
                <Card.Body>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter Name"
                                value={employee.name}
                                onChange={(e) => { setEmployee({ ...employee, name: e.target.value }) }}
                            />
                            <Form.Label>Company</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter Company"
                                value={employee.cname}
                                onChange={(e) => { setEmployee({ ...employee, cname: e.target.value }) }}
                            />
                            <Form.Label>Image</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter image Url"
                                value={employee.avatar}
                                onChange={(e) => { setEmployee({ ...employee, avatar: e.target.value }) }}
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit" className="home-submit-button">
                            Submit
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </Container >
    );
}


