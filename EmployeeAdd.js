import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';



export default function EmployeeAdd() {
    const [employee, setEmployee] = useState({});
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch("https://64952386b08e17c91791adac.mockapi.io/emp", {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(employee)
        })
            .then(() => {
                navigate("/");
            })
            .catch((error) => {
                console.log(error);
            });
    }

    return (
        <div className="employee-add-container">
            <Card className="employee-card">
                <Card.Img variant="top" src={employee.avatar} />
                <Card.Body>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="name"
                                placeholder="Enter Name"
                                onChange={(e) => { setEmployee({ ...employee, name: e.target.value }) }}
                            />
                            <Form.Label>Company</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter Company"
                                value={employee.cname}
                                onChange={(e) => { setEmployee({ ...employee, cname: e.target.value }) }}
                            />
                            <Form.Label>Image</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter Image URL"
                                value={employee.avatar}
                                onChange={(e) => { setEmployee({ ...employee, avatar: e.target.value }) }}
                            />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </div>
    );
}
