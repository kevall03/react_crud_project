import React from 'react';
import { Outlet } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';



export default function Header() {
  return (
    <div>
      <Navbar className="bg-custom-color">
        <Container>
          <Navbar.Brand href="#home" className="header-brand">
            Employees
          </Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text className="header-text">
              Signed in as: <a>21030401053</a>
            </Navbar.Text>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <div>
        <Outlet />
      </div>
    </div>
  );
}
