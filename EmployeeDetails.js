import React, { useEffect, useState } from 'react';
import { Outlet, useNavigate, useParams } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';



export default function EmployeeDetails() {
    const [employee, setEmployee] = useState({});
    const navigate = useNavigate();
    const { id } = useParams();

    useEffect(() => {
        console.log('Fetching employee data for ID:', id);
        fetch(`https://64952386b08e17c91791adac.mockapi.io/emp/${id}`)
            .then((res) => res.json())
            .then((data) => setEmployee(data));
    }, [id]);

    const handleDelete = () => {
        fetch(`https://64952386b08e17c91791adac.mockapi.io/emp/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(employee),
        })
            .then(() => {
                navigate('/');
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const handleEdit = () => {
        navigate(`/empedit/${id}`);
    };

    return (
        <div className="employee-details-container">
            <Card className="employee-card">
                <Card.Img variant="top" src={employee.avatar} />
                <Card.Body>
                    <Card.Title>{employee.name}</Card.Title>
                    <Card.Text>{employee.cname}</Card.Text>
                    <div className="button-container">
                        <Button variant="primary" onClick={handleEdit}>
                            Edit
                        </Button>
                        <Button variant="danger" onClick={handleDelete}>
                            Delete
                        </Button>
                    </div>
                </Card.Body>
            </Card>
            <Outlet />
        </div>
    );
}
